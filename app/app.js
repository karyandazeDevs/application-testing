'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('myApp', [
  'ngRoute',
  'ui.bootstrap',
    'angular-perfect-scrollbar-2',
    'modelService',
    'productFeedService',
    'ngAnimate',
    'angular-preload-image',
  'myApp.productFinder',
    'myApp.resultsView',
  'myApp.version',
  'rzModule'
]);
app.config(['$routeProvider', function($routeProvider) {
      $routeProvider.otherwise({redirectTo: '/productFinder'});
}]);
app.run(['productFeedService', function(productFeedService) {
    productFeedService.init();
}]);
