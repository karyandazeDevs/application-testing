'use strict';

function findById(arr,id){
  return arr.filter(function(item){
    return item._id == id;
  })[0];
}

function getNextSlide(scope,optionId){
    var current = {};
    current.slide = scope.currentSlide;
    current.option = findById(scope.optionsList, optionId);
    var pathToNextSlide = current.slide.shortcut;
    // go through slidesList and find the one with the path that include the path, this path is the only one as slide is uniquely defined by the (prevSlide, selectedOptionId).
    var nextSlide = scope.slidesList.filter(function(item){
         return (item.previousSlide === current.slide._id && (item.path[pathToNextSlide] === current.option.shortcut || item.path[pathToNextSlide] === "Any"));
    })[0];
    console.log(nextSlide);
    return nextSlide;
}

function getProducts(actionObject,productList){
    return productList.filter(function(product){
        return actionObject.attributes.filter(function(attribute){
            return product.attributes.indexOf(attribute) != -1;
        }).length != 0;
    });
}

angular.module('myApp.productFinder', ['ngRoute','ngCookies'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/productFinder', {
    templateUrl: 'productFinder/productFinder.html',
    controller: 'ProductFinderCtrl'
  });
}])

.controller('ProductFinderCtrl',
  function($scope,$http,modelService,$location, $timeout, $cookies, productFeedService, $rootScope) {

      $scope.actionStack = []; // for navigation in breadcrumb, stores slide and selected by user option that led to next slide
      $scope.products = []; // products to display on last page
      $scope.findById = findById;
      $scope.productFeed = [];
      $scope.showProductFeed = false;
      $scope.productFeedContainerScrollOpts = {
          useBothWheelAxes:"true",
          suppressScrollX:"true"
      };
      $scope.productList = [];
      $scope.endOfPath = false;
      $scope.endSlide = {_id:"THISISTHEEND",shortcut:"Produkte",text:"Unsere Empfehlungen für sie"};
      $scope.productDisplayedLimit = 4;
      $scope.slider = {min: 0, max: 1000, options: {floor: 0, ceil: 1000, hideLimitLabels: true, translate: function(value, sliderId, label) {
      switch (label) {
        case 'model':
          return '<b>Min.Preis:</b> €' + value;
        case 'high':
          return '<b>Max.Preis:</b> €' + value;
        default:
          return '€' + value
      }
    }
  }
};


      $scope.$watch("endOfPath",function(newVal){
         if(newVal === true){
             //fetch path and supply it to productFeedService as a filter
            console.log(newVal);
            var filter = {};
            // console.log("Filter.initial",filter);
            $scope.actionStack.forEach(function(obj){
              console.log('--------------------------');
               console.log($scope.actionStack, obj);
               for(var key in obj.path){
                     console.log(key);
                     filter[key] = obj.path[key];
               }
            });
            console.log("Filter.step1", filter);
            //FIXME: temporarilsy update filter to fit CSV fields names. remove it later when CSV is fixed
            filter = {"geschlecht": filter["Für wen"],"farbe":filter.Farbe, feature: filter["Feature"], bauart: filter["Feature1"], liegeflaeche: filter['Bettgröße'], 'Betthöhe': filter['Betthöhe']};
            //"liegeflaeche":filter.Bettgröße, "suchbegriffe":filter.Feature
            console.log("Filter.step2",filter);
            productFeedService.getProducts(filter, function(products){
              $scope.products = products;
              console.log($scope.products);
              var newCeilValue = Math.max.apply(Math,$scope.products.map(function(product){return product.price;}));
            //  $scope.slider = {min: 0, max: newCeilValue, options: {floor: 0, ceil: newCeilValue}};
              $scope.slider.max = Math.ceil(newCeilValue);
              $scope.slider.options.ceil = Math.ceil(newCeilValue);
              $scope.refreshSlider();
              console.log("putCookies on endPath watch");
              cookieManager.putCookies();
              $scope.productManager.showMoreProducts();
            });
         }
      });

      $scope.refreshSlider = function () {
           $timeout(function () {
           $scope.$broadcast('rzSliderForceRender');
           });
       };

      var cookieManager = function(){
          var cookieKey = "sb-shoepassion";
          var valuesToStore = ["actionStack","currentSlide","endOfPath"];
          var putCookies = function(){
              var cookieObject = {};
              valuesToStore.forEach(function(item){
                 cookieObject[item] = $scope[item];
                  $cookies.putObject(cookieKey,cookieObject);
              });
          };
          var getCookies = function(){
              var cookieObject = $cookies.getObject(cookieKey);
              if(cookieObject){
                  valuesToStore.forEach(function(item){
                        if(item in cookieObject){
                            $scope[item] = cookieObject[item];
                        }
                  });
              }
              removeCookies();
          };
          var removeCookies = function(){
              $cookies.remove(cookieKey);
          }
          return {
              putCookies: putCookies,
              getCookies: getCookies,
              removeCookies: removeCookies
          }
      }();

      $scope.shouldDisplayInBreadcrumb = function(action){
          return findById($scope.slidesList,action.slideId).hasOwnProperty('breadcrumbHide') ? !findById($scope.slidesList,action.slideId).breadcrumbHide : true;
      };

      $scope.getDisplayedActions = function(){
          return $scope.actionStack.filter(function(item){
                return $scope.shouldDisplayInBreadcrumb(item);
          });
      };

      function changeSlideTo(slideId,reverse){
          if(reverse){
              $scope.animationSwap = "swap-animation-reverse";
              if($scope.endOfPath){
                  $scope.endOfPath = false;
                  $scope.products = [];
                  cookieManager.removeCookies();
                  $scope.productManager.reset();
              }
          }
          //FIXME might there be a better solution than timeout to wait for the class to be added?
          $timeout(function(){
              $scope.currentSlide = findById($scope.slidesList,slideId);
          },0);
      }

      $scope.previous = function(){
          var curAction = $scope.actionStack.pop();
          changeSlideTo(curAction.slideId,true);
      };
      $scope.inRange = function(prop, val1, val2){
              return function(product){
              return product[prop] > val1 && product[prop] < val2;
    }
}
      $scope.gridController = {
          getNumberOfRow: function(collectionLength){
              return Math.ceil( collectionLength / 6);
          },
          getItemsPerLine: function(collectionLength){
              return Math.min( Math.round(collectionLength / this.getNumberOfRow(collectionLength)),6)
          },
          getItemSize: function(collectionLength){
              return Math.round( 12 / this.getItemsPerLine(collectionLength));
          },
          getItemOffSize: function(index, collectionLength){
              return index % this.getItemsPerLine(collectionLength)  == 0 ? ( 12 % Math.min(collectionLength,this.getItemsPerLine(collectionLength)))/2 : 0;
          }
      };

      $scope.onProductFeedUpdate = function(){
      };

      $scope.goBackToAction = function(action){
        console.log(action, $scope.actionStack, $scope.actionStack.indexOf(action));
        $scope.actionStack = $scope.actionStack.slice(0,$scope.actionStack.indexOf(action));
        changeSlideTo(action.slideId,true);
      };

      $http.get("data_test/kids.json")
          .then(function(response) {
              $scope.optionsList = response.data.optionsList;
              console.log($scope.optionsList);
              $scope.slidesList = response.data.slidesList;
              $scope.slidesList.push($scope.endSlide);
              $scope.initialSlide = response.data.initialSlide;
              $scope.defaultPath = response.data.defaultPath;
              $scope.defaultPath.push($scope.endSlide._id);
              $scope.currentSlide = findById($scope.slidesList,$scope.initialSlide);
              console.log($scope.currentSlide);
              cookieManager.getCookies();
              console.log($scope.endOfPath);
          });

      // TODO Remove when implementation is correct
     // $http.get("data_test/test_data_productFeed.json")
     //     .then(function(response) {
      //        $scope.productList = response.data;
      //    });
      // END to be removed

      $scope.optionClicked = function(optionId){
          //Convert optionId to string since json is using strings
          optionId = "" + optionId;
          $scope.animationSwap = " swap-animation";
          var oldSlideId = $scope.currentSlide._id;
          var actionPath = {};
          actionPath[$scope.currentSlide.shortcut] = findById($scope.optionsList,optionId).shortcut;
          var actionObject = {"slideId" : oldSlideId, "path": actionPath, "option":"" + optionId, "attributes": findById($scope.optionsList,optionId).attributes};
          // console.log(productFeedService.getPossibleValues(["oberflaeche","farbe", "material", "alter", "geschlecht", "liegeflaeche", "besondereeigenschaften", "motive", "suchbegriffe"]));
          productFeedService.getProducts({"suchbegriffe":"Schlupfsprossen","geschlecht":"Junge"}, function(products){console.log(products);});
          if(getNextSlide($scope,optionId) === undefined){ // ELENA FIXME
              $scope.actionStack.push(actionObject);
              $scope.endOfPath = true;
              console.log("putCookies on optionClicked");
              cookieManager.putCookies();

              /*
              var data = modelService.get();
              data.actionStack = [];
              angular.copy($scope.actionStack,data.actionStack);
              modelService.set(data);
              $location.path("/results");*/
          } else {
            //FIXME might there be a better solution than timeout to wait for the class to be added?
              $timeout(function(){
                  console.log($scope, optionId);
                  $scope.currentSlide = getNextSlide($scope,optionId);

                  //TODO send actionObject
                  $scope.productFeed = getProducts(actionObject,$scope.productList);
                  $scope.productFeedContainerUpdate = true;
                  $scope.actionStack.push(actionObject);
                  $scope.products = [];
              },0);
          }
      };

      $scope.productManager = function(){
          var index = 0;
          var reset = function(){
              index = 0;
          };
          var hasMore = function(){
              if($scope.products){
                  return index <= $scope.products.length;
              } else {
                  return false;
              }
          };

          var showMore = function(){
              if($scope.products){
                  $scope.shownProducts = $scope.products.slice(index,index + $scope.productDisplayedLimit);
                  index += $scope.productDisplayedLimit;
              }
          };

          return {
              hasMoreProducts: hasMore,
              showMoreProducts: showMore,
              reset: reset
          };
      }();
});
